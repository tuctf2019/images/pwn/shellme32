# shellme32 -- PWN -- TUCTF2019

[Source](https://gitlab.com/tuctf2019/pwn/shellme32)

## Chal Info

Desc: `A hacker's first always bring me to tears.`

Given files:

* shellme32

Hints:

* Have you checked the memory permissions?

Flag: `TUCTF{4www..._b4by5_f1r57_3xpl017._h0w_cu73}`

## Deployment

[Docker Hub](https://hub.docker.com/r/asciioverflow/shellme32)

Ports: 8888

Example usage:

```bash
docker run -d -p 127.0.0.1:8888:8888 asciioverflow/shellme32:tuctf2019
```
